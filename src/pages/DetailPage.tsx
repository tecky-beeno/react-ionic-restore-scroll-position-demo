import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useParams } from 'react-router'
import ExploreContainer from '../components/ExploreContainer'

const DetailPage: React.FC = () => {
  const params = useParams()
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton></IonBackButton>
          </IonButtons>
          <IonTitle>Detail page</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">{JSON.stringify(params)}</IonContent>
    </IonPage>
  )
}

export default DetailPage
