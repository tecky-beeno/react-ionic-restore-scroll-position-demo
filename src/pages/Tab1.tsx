import {
  IonButton,
  IonCard,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRange,
  IonTitle,
  IonToolbar,
  useIonViewDidLeave,
  useIonViewWillEnter,
  useIonViewWillLeave,
} from '@ionic/react'
import { useEffect, useLayoutEffect, useState } from 'react'
import './Tab1.css'

type Range = {
  lower: number
  upper: number
}

type SearchResultItem = {
  id: number
  price: number
  name: string
}

async function search(priceRange: Range): Promise<SearchResultItem[]> {
  const list: SearchResultItem[] = []
  for (let i = 0; i < 10; i++) {
    let id = Math.floor(Math.random() * 1000)
    while (list.find(item => item.id === id)) {
      id++
    }
    let price = +(
      Math.random() * (priceRange.upper - priceRange.lower) +
      priceRange.lower
    ).toFixed(1)
    let name = 'item - ' + id
    list.push({
      id,
      price,
      name,
    })
  }
  return list
}

const Tab1: React.FC = () => {
  const min = 0
  const max = 100_000
  const [priceRange, setPriceRange] = useState<Range>({
    lower: min,
    upper: max,
  })
  const [list, setList] = useState<SearchResultItem[]>([])
  const [error, setError] = useState('')
  const [ionContent, setIonContent] = useState<HTMLIonContentElement | null>()
  const [innerScroll, setInnerScroll] = useState<Element | null>()
  const [scrollTop, setScrollTop] = useState(0)

  function saveScrollPosition() {
    console.log('saveScrollPosition')
    console.log('innerScroll:', innerScroll)
    if (!innerScroll) return
    console.log('scrollTop:', innerScroll.scrollTop)
    setScrollTop(innerScroll.scrollTop)
  }

  useIonViewWillEnter(() => {
    console.log('will enter, restore scrollTop:', scrollTop)
    if (!innerScroll) return
    innerScroll.scrollTop = scrollTop
  }, [scrollTop, innerScroll])

  useLayoutEffect(() => {
    if (!ionContent) return
    setTimeout(() => {
      let innerScroll = ionContent.shadowRoot!.querySelector('.inner-scroll')
      setInnerScroll(innerScroll)
    })
  }, [ionContent])

  useEffect(() => {
    setError('')
    search(priceRange).then(setList).catch(setError)
  }, [priceRange])

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen ref={setIonContent}>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonLabel position="stacked">Price Range</IonLabel>
          <IonRange
            dualKnobs
            step={1000}
            pin
            value={priceRange}
            min={min}
            max={max}
            onIonChange={e => {
              let range = e.detail.value as Range
              if (
                range.upper === priceRange.upper &&
                range.lower === priceRange.lower
              ) {
                return
              }
              console.log(range)
              setPriceRange(range as Range)
            }}
          ></IonRange>
        </IonItem>
        <p>{JSON.stringify(priceRange)}</p>
        <p hidden={!error} style={{ color: 'red' }}>
          {JSON.stringify(error)}
        </p>
        <IonList>
          {list.map(item => (
            <IonCard key={item.id} className="ion-padding">
              <h2>{item.name}</h2>
              <p>HKD $ {item.price}</p>
              <IonButton
                onClick={saveScrollPosition}
                routerLink={`/item/${item.id}`}
              >
                Detail
              </IonButton>
            </IonCard>
          ))}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default Tab1
